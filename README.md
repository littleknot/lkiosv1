# Repo
* [https://github.com/magnuky/lk-android-export-contacts](https://github.com/magnuky/lk-android-export-contacts)
* [https://github.com/magnuky/lk-ios-import-guest](https://github.com/magnuky/lk-ios-import-guest)

# Mock Up and User Flow
* [https://dl.dropboxusercontent.com/u/424651/lkfv2/mockup/start.html#p=export_contacts--ip6_375](https://dl.dropboxusercontent.com/u/424651/lkfv2/mockup/start.html#p=export_contacts--ip6_375)
* [https://dl.dropboxusercontent.com/u/424651/lkfv2/mockup/start.html#p=export_contacts--s5_360](https://dl.dropboxusercontent.com/u/424651/lkfv2/mockup/start.html#p=export_contacts--s5_360)
* [http://bit.ly/lkfv2-ios](http://bit.ly/lkfv2-ios)
* [http://bit.ly/lkfv2-android_duplicate_name](http://bit.ly/lkfv2-android_duplicate_name)

