//
//  ContactCell.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 1/22/15.
//  Copyright (c) 2015 Littleknot. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    
    @IBOutlet weak var checkIcon: UILabel!
    @IBOutlet weak var itemLabel: UILabel!

    @IBOutlet weak var itemLabelName: UILabel!
    @IBOutlet weak var checkIconName: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var conflictMessage: UILabel!
    @IBOutlet weak var conflictSign: UILabel!
    @IBOutlet weak var conflictNameMessage: UITextView!
    @IBOutlet weak var conflictNameSign: UILabel!
    @IBOutlet weak var cellDescription: UILabel!
    
    @IBOutlet weak var ccodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}