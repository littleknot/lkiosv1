//
//  Contact.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/13/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

class Contact{
    lazy var fname = NSString()
    lazy var lname = NSString()
    lazy var names = [NSString]()
    lazy var emails = [NSString]()
    lazy var phones = [NSString]()
    lazy var ccode = ""
    lazy var merged = false
    lazy var recordID = Int()
    lazy var guestID = NSString()
    
    init(){}
    
    func getName() -> String{
        return "\(self.fname) \(self.lname)"
    }
}
