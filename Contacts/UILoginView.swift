//
//  LoginView.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/13/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

class UILoginView: UIViewController, FBLoginViewDelegate{
    
    var settings: Settings!
    var loginShown: Bool!
    var fbID: String!
    
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet var fbLoginView: FBLoginView?
    @IBOutlet weak var reconnectButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var loader: UILabel!
    
    @IBAction func reconnectPressed(sender: AnyObject) {
        self.fetchGuests()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.settings = Settings()
        self.fbLoginView?.delegate = self
        self.view.backgroundColor = UIColorFromHex(0x26C6DA, alpha: 1)
        self.activityIndicator.hidesWhenStopped = true
        self.fbLoginView?.readPermissions = ["public_profile", "email"]
    }
    
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        self.activityIndicator.startAnimating()
        self.fbLoginView?.hidden = true
        self.loginShown = true
    }

    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
        if (self.loginShown != nil) && (self.loginShown == true){
            
            self.fbID = user.objectID
            //var userEmail = user.objectForKey("email") as String
            if self.fbID != nil{
                self.fetchGuests()
            }
            NSUserDefaults.standardUserDefaults().setObject(user.objectID, forKey: "fbidkey")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        self.loginShown = false
    }
    
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        //        println("User Logged Out")
    }
    
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        //        println("Error: \(handleError.localizedDescription)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchGuests() {
        self.activityIndicator.startAnimating()
        self.reconnectButton.hidden = true
        self.errorMessage.hidden = true
        connection.auth(self.settings.authURL, fbid: self.fbID){
            (response) in
            
            if (response["error"] != nil){
                println("error")
                dispatch_async(dispatch_get_main_queue()){
                    self.errorMessage.text = response["error"] as? String
                    self.activityIndicator.stopAnimating()
                    self.errorMessage.hidden = false
                    self.reconnectButton.hidden = false
                    self.fbLoginView?.hidden = false
                }
            }else{
                if let userObject = response["user"] as? NSDictionary{
                    if let userKey = userObject["_id"] as? NSString{
                        println("User ID: \(userKey)")
                        NSUserDefaults.standardUserDefaults().setObject(userKey as String, forKey: "userKey")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                }
                if let accountObject = response["account"] as? NSDictionary{
                    if let accountID = accountObject["_id"] as? NSString{
                        println("Account ID: \(accountID)")
                        NSUserDefaults.standardUserDefaults().setObject(accountID as String, forKey: "accountKey")
                        NSUserDefaults.standardUserDefaults().synchronize()
                    }
                }
                
                if let guests = response["guests"] as? NSArray{
                    for item in guests{
                        var guestObject = Guest()
                        guestObject.id        = item["_id"] as String
                        if let fname = item["fname"] as? NSNull{
                            println("fname is null")
                        }else{
                            guestObject.fname     = item["fname"] as String
                        }
                        if let lname = item["lname"] as? NSNull{
                            println("lname is null")
                        }else{
                            guestObject.lname     = item["lname"] as String
                        }
                        if let email = item["email"] as? NSNull{
                            println("email is null")
                        }else{
                            guestObject.email     = item["email"] as String
                        }
                        if let country_code = item["country_code"] as? NSNull{
                            println("country code is null")
                        }else{
                            guestObject.ccode     = item["country_code"] as String
                        }
                        if let mobile = item["mobile"] as? NSNull{
                            println("mobile is null")
                        }else{
                            guestObject.phone     = item["mobile"] as String
                        }
                        if let ios_contact_id = item["ios_contact_id"] as? NSNull{
                            println("IO Contact ID is null")
                        }else{
                            guestObject.contactID = item["ios_contact_id"] as String
                            println("Contact ID isnt null: \(guestObject.fname) \(guestObject.contactID)")
                        }
                        guestCollection.addGuest(guestObject)
                    }
                }
                self.activityIndicator.stopAnimating()
                self.reconnectButton.hidden = true
                self.errorMessage.hidden = true
                self.fbLoginView?.hidden = false
                self.performSegueWithIdentifier("showContacts", sender: self)
            }
        }
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}
