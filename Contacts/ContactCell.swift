//
//  ContactCell.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 1/22/15.
//  Copyright (c) 2015 Littleknot. All rights reserved.
//

import UIKit

class ContactCell: UITableViewCell {
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusIcon: UILabel!
    @IBOutlet weak var emailIcon: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var mobileIcon: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
        mobileIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
        emailIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
        statusIcon.textColor = UIColorFromHex(0xB7AEAC, alpha: 1)
        bottomBar.hidden = true
        activityIndicator.hidesWhenStopped = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
}