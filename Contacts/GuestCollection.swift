//
//  GuestCollection.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/15/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

var guestCollection: GuestCollection = GuestCollection()
var guest = Guest()

class GuestCollection: NSObject{
    var guests = [Guest]()
    
    var settings: Settings!
    
    override init(){
        self.settings = Settings()
    }
    
    func addGuest(guestObject: Guest){
        println("Added \(guestObject.fname)")
        guests.append(guestObject)
    }
    
    func removeGuest(guestID: String){
        for(var x = 0; x < guests.count; x++){
            if guests[x].id == guestID{
                println("Removed \(guests[x].fname) at \(x)")
                guests.removeAtIndex(x)
            }
        }
    }
    
    func getGuestID(recordID: Int) -> String{
        var guestID = ""
        for guest in guests{
            if guest.contactID.toInt() == recordID{
                return guest.id
            }
        }
        return guestID
    }
    
    func hasTheSameName(contact: Contact, guest: Guest) -> Bool{
        return contact.getName() == guest.getName()
    }
    
    func hasTheSameMobile(contact: Contact, guest: Guest) -> Bool{
        for phone in contact.phones{
            println("\(phone) : \(guest.ccode)\(guest.phone)")
            if phone == "\(guest.ccode)\(guest.phone)"{
                return true
            }
        }
        return false
    }
    
    func hasTheSameEmail(contact: Contact, guest: Guest) -> Bool{
        for email in contact.emails{
            if email == guest.email{
                return true
            }
        }
        return false
    }
    
    func hasTheSameRecord(contact: Contact, guest: Guest) -> Bool{
//        println("contact: \(contact.getName()) guest: \(guest.getName())")
        if (self.hasTheSameName(contact, guest: guest) || self.hasTheSameEmail(contact, guest: guest) || self.hasTheSameMobile(contact, guest: guest)) {
            return true
//            if self.hasTheSameName(contact, guest: guest){
//                if hasTheSameMobile(contact, guest: guest) && hasTheSameMobile(contact, guest: guest){
//                    return true
//                }
//            }
        }
        return false
    }
    
    func getGuestByContact(contact: Contact) -> Guest{
        var guest = Guest()
        for item in guests{
            if item.contactID.toInt() == contact.recordID{
                println("Yes, \(guest.contactID.toInt()) is added: \(contact.recordID) - \(guest.fname)")
                guest = item
            } else {
                if self.hasTheSameRecord(contact, guest: item){
                    guest = item
                }
            }
        }
        return guest
    }
    
    func fetchGuests(callback: (NSDictionary) -> ()){
        var userKey = NSUserDefaults.standardUserDefaults().objectForKey("userKey") as String!
        var accountKey = NSUserDefaults.standardUserDefaults().objectForKey("accountKey") as String!
        if (userKey != nil) && (accountKey != nil){
            connection.get(settings.guestsURL, userKey: userKey, accountKey: accountKey, callback: callback)
        }
    }
    
    func performSync(guestObject: Guest, action: String, callback:(NSDictionary) -> ()){
        var userKey = NSUserDefaults.standardUserDefaults().objectForKey("userKey") as String!
        var accountKey = NSUserDefaults.standardUserDefaults().objectForKey("accountKey") as String!
        if (userKey != nil) && (accountKey != nil){
            if action == "Add"{
                connection.post(["fname": guestObject.fname, "lname": guestObject.lname, "email": guestObject.email, "mobile": guestObject.phone, "ios_contact_id": guestObject.contactID, "country_code": guestObject.ccode], userKey: userKey, accountKey: accountKey, url: settings.guestsURL, callback: callback)
            } else if action == "Merge" {
                println("GUESTID: \(guestObject.id)")
                var url = "\(settings.guestURL)/\(guestObject.id)"
                connection.patch(["email": guestObject.email, "mobile": guestObject.phone, "ios_contact_id": guestObject.contactID, "country_code": guestObject.ccode], userKey: userKey, accountKey: accountKey, url: url, callback: callback)
            }
        }
    }
}