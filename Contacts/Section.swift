//
//  Section.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 12/3/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

class Section{
    var items: [Item] = []
    
    func addItem(item: Item){
        self.items.append(item)
    }
}