//
//  GRoup.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/13/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit
import AddressBook

class Group{
    var name: String
    var id: ABRecordID!
    var selected: Bool
    var obj: ABRecordRef!
    
    init(name:String){
        self.name = name
        self.selected = false
    }
    
}
