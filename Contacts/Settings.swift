//
//  Settings.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/15/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import Foundation

class Settings{
    
    var authURL:String!
    var guestsURL:String!
    var guestURL:String!
    var APIKey:String!
    
    init(){
        var env = "production"
        
        var rootURL = ""
        
        if env == "development" {
            rootURL              = "http://localhost:3000"
            self.APIKey          = "f7e0992b2cda3088d414fddbedb59566"
        }else if env == "staging" {
            rootURL              = "http://staging.littleknot.com"
            self.APIKey          = "3810bfb63e1c0541b8b307faab373e5c"
        }else if env == "production" {
            rootURL              = "https://www.littleknot.com"
            self.APIKey          = "3810bfb63e1c0541b8b307faab373e5c"
        }
//        self.authURL             = rootURL + "/mobile/2.0/user/token"
//        self.guestsURL           = rootURL + "/mobile/2.0/guests"
//        self.guestURL            = rootURL + "/mobile/2.0/guest"
          self.authURL             = rootURL + "/mobile/special/init-fb-user"
          self.guestsURL           = rootURL + "/mobile/api/guests"
          self.guestURL            = rootURL + "/mobile/api/guests"
    }
    
    func alertMessage(type: String) -> String{
        if type == "unreachable"{
            return "Something went wrong. Please try again later"
        }else if type == "noConnection"{
            return "You are not connected to the internet. Please check your connection settings."
        }
        return ""
    }
}