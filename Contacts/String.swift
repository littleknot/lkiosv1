//
//  String.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/16/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import Foundation

extension String {
    public func split(separator: String) -> [String] {
        if separator.isEmpty {
            return map(self) { String($0) }
        }
        if var pre = self.rangeOfString(separator) {
            var parts = [self.substringToIndex(pre.startIndex)]
            while let rng = self.rangeOfString(separator, range: pre.endIndex..<endIndex) {
                parts.append(self.substringWithRange(pre.endIndex..<rng.startIndex))
                pre = rng
            }
            parts.append(self.substringWithRange(pre.endIndex..<endIndex))
            return parts
        } else {
            return [self]
        }
    }
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}