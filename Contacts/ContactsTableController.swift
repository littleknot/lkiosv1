//
//  ContactsTableController.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/13/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit
import AddressBook
import CoreData

let managedObjectContext = (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext

class ContactsTableController: UITableViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, DataSelectGroupDelegate, DataMergedDelegate {
    
    @IBOutlet var contactsCollection: UITableView!
    
    @IBAction func signOutButton(sender: AnyObject) {
        FBSession.activeSession().closeAndClearTokenInformation()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    var adbk : ABAddressBook!
    var contacts = [Contact]()
    var filteredContacts = [Contact]()
    var groupsFilteringMode = false;
    
    /* type to represent table items
    `section` stores a `UITableView` section */
    
    let collation = UILocalizedIndexedCollation.currentCollation() as UILocalizedIndexedCollation
    var _sections: [Section]?
    var sections: [Section] {
        if self._sections != nil{
            return self._sections!
        }
        
        var items: [Item] = self.contacts.map{ contact in
            var item = Item(name: contact.fname + " " + contact.lname, fname: contact.fname, lname: contact.lname, phones: contact.phones, emails: contact.emails, recordID: contact.recordID, merged: contact.merged)
            item.guestID = contact.guestID
            item.names = contact.names
            item.section = self.collation.sectionForObject(item, collationStringSelector: "name")
            return item
        }
        
        var sections = [Section]()
        
        for i in 0..<self.collation.sectionIndexTitles.count{
            sections.append(Section())
        }
        
        for item in items{
            sections[item.section!].addItem(item)
        }
        
        for section in sections{
            section.items = self.collation.sortedArrayFromArray(section.items, collationStringSelector: "name") as [Item]
        }
        
        self._sections = sections
        
        return self._sections!
    }
    
    func cleanHeaders(){
        self._sections = nil
        var sections = [Section]()
        self._sections = sections
    }
    func reloadContent(){
        if !self.groupsFilteringMode{
            self._sections = nil
            
            var items: [Item] = self.contacts.map{ contact in
                var item = Item(name: contact.fname + " " + contact.lname, fname: contact.fname, lname: contact.lname, phones: contact.phones, emails: contact.emails, recordID: contact.recordID, merged: contact.merged)
                item.guestID = contact.guestID
                item.names = contact.names
                item.section = self.collation.sectionForObject(item, collationStringSelector: "name")
                return item
            }
            
            var sections = [Section]()
            
            for i in 0..<self.collation.sectionIndexTitles.count{
                sections.append(Section())
            }
            
            for item in items{
                sections[item.section!].addItem(item)
            }
            
            for section in sections{
                section.items = self.collation.sortedArrayFromArray(section.items, collationStringSelector: "name") as [Item]
            }
            self._sections = sections
        }
    }
    
    // Delegate methods
    
    func userDidMergeContactInformation(controller: ImportViewController, guest: Guest, selectedPath: NSIndexPath, action: String, mergeMode: String){
        
        for (index, item) in enumerate(self.contacts){
            if "\(item.recordID)" == guest.contactID{
                self.contacts[index].merged = true
                self.contacts[index].guestID = guest.id
            }
        }
        performSync(self.tableView, record: guest, indexPath: selectedPath, mode: "contacts", action: action, mergeMode: mergeMode)
        
        dispatch_async(dispatch_get_main_queue()){
            if mergeMode != "search"{
                self.reloadContent()
                self.contactsCollection.reloadData()
            }
            
        }
        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    func userDidSelectGroup(controller: GroupsTableController, record: ABRecord?, showFiltered: Bool){
        self.groupsFilteringMode = showFiltered
        if record != nil{
            if let CFPeople = ABGroupCopyArrayOfAllMembers(record){
                let people = CFPeople.takeRetainedValue() as NSArray as [ABRecordRef]
                self.getContactNames(people)
            }else{
                self.contacts = []
                self.reloadContent()
            }
        }else{
            self.groupsFilteringMode = false
            self.getAllContactNames()
        }
        
        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    
    // Overrides
    override func viewDidLoad() {
        super.viewDidLoad()

//        self.navigationController!.navigationBar.setBackgroundImage(navigationLogo, forBarMetrics:.Default)
        // Create your image
        var navigationLogo = UIImage(named: "Logo");
        var imageview = UIImageView(frame: CGRect(x: 0, y: 26, width:26, height: 26));
        imageview.image = navigationLogo;
        imageview.contentMode = UIViewContentMode.ScaleAspectFit;

        // set the text view to the image view
        self.navigationItem.titleView = imageview;
        self.navigationController?.navigationBar.translucent = false;
        self.contactsCollection.sectionIndexColor = UIColorFromHex(0x939393, alpha: 1)
        
        if Reachability.isConnectedToNetwork() {
            if self.determineStatus(){
                self.getAllContactNames()
            }
        } else {
            let alertController = UIAlertController(title: "No Internet Connection", message: guestCollection.settings.alertMessage("noConnection"), preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        if (self.tableView != self.searchDisplayController!.searchResultsTableView){
            self.contactsCollection.reloadData()
        }
    }
    
    func getAllContactNames(){
        let people = ABAddressBookCopyArrayOfAllPeople(self.adbk).takeRetainedValue() as NSArray as [ABRecord]
        self.getContactNames(people)
    }
    
    func getContactByGuestID(guestID: NSString) -> Int{
        for (index, item) in enumerate(self.contacts){
            println("\(item.guestID) == \(guestID)")
            if item.guestID == guestID{
                return index
            }
        }
        return 0
    }
    
    func getContactByContactID(contactID: NSString) -> Int{
        for (index, item) in enumerate(self.contacts){
            println("\(item.recordID) == \(contactID)")
            if item.guestID == contactID{
                return index
            }
        }
        return 0
    }
    
    // Guest list of contacts from addressbook
    func getContactNames(people: NSArray){
        self.contacts = []
        for person in people {
            
            let recordID = Int(ABRecordGetRecordID(person) as ABRecordID) as Int
            let fname = ABRecordCopyValue(person, kABPersonFirstNameProperty).takeRetainedValue() as NSString
            let lname = ABRecordCopyValue(person, kABPersonLastNameProperty).takeRetainedValue() as NSString
            
            var contact = Contact()
            contact.recordID = recordID
            contact.fname = fname
            contact.lname = lname
            
            let emails:ABMultiValueRef = ABRecordCopyValue(person, kABPersonEmailProperty).takeRetainedValue()
            let emailsCount = ABMultiValueGetCount(emails)
            
            for var i = 0; i < emailsCount; ++i {
                let emailItem:ABMultiValueRef = ABMultiValueCopyValueAtIndex(emails, i).takeRetainedValue()
                if isValidEmail(emailItem as String){
                    contact.emails.append(emailItem as String)
                }
            }
            
            let numbers:ABMultiValueRef = ABRecordCopyValue(person, kABPersonPhoneProperty).takeRetainedValue()
            let numbersCount = ABMultiValueGetCount(numbers)
            
            for var i = 0; i < numbersCount; ++i {
                let numberItem:ABMultiValueRef = ABMultiValueCopyValueAtIndex(numbers, i).takeRetainedValue()
                var number = numberItem as String
                number = number.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                number = number.stringByReplacingOccurrencesOfString(")", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                number = number.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                number = number.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                contact.phones.append(number)
            }
            
            contact.names.append("\(contact.fname) \(contact.lname)")
            var guest = guestCollection.getGuestByContact(contact)
            contact.ccode = guest.ccode
            if guest.isValid(){
                contact.names.append(guest.getName())
                contact.guestID = guest.id
                if !guest.contactID.isEmpty{
                    if "\(contact.recordID)" == "\(guest.contactID)"{
                        contact.merged = true
                    }
                    println("\(contact.fname) \(contact.recordID) \(guest.contactID)")
                }
            } else {
                contact.merged = false
            }
            
            self.contacts.append(contact)
        }
        dispatch_async(dispatch_get_main_queue()){
            
            self.reloadContent()
            self.contactsCollection.reloadData()
        }
    }
    
    // Creates and addressbook object if there is none
    func createAddressBook() -> Bool {
        if self.adbk != nil {
            return true
        }
        var err : Unmanaged<CFError>? = nil
        let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
        if adbk == nil {
            println(err)
            self.adbk = nil
            return false
        }
        self.adbk = adbk
        return true
    }
    
    func filterContentForSearchText(searchText: String) {
        // Filter the array using the filter method
        self.filteredContacts = self.contacts.filter({( contact: Contact) -> Bool in
            let fnameMatch = contact.fname.rangeOfString(searchText)
            let lnameMatch = contact.lname.rangeOfString(searchText)
            let fullnameMatch = "\(contact.fname) \(contact.lname)".rangeOfString(searchText)
            return (fnameMatch.location != NSNotFound) || (lnameMatch.location != NSNotFound) || (fullnameMatch != nil)
        })
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        self.filterContentForSearchText(searchString)
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        return true
    }
    
    func searchDisplayControllerDidEndSearch(controller: UISearchDisplayController!) -> Bool{
        self.reloadContent()
        dispatch_async(dispatch_get_main_queue()){
            self.contactsCollection.reloadData()
        }
        return true
    }
    
    // UITableView
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if !self.groupsFilteringMode{
            if tableView != self.searchDisplayController!.searchResultsTableView {
                return self.sections.count
            }
        }
        return 1
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if !self.groupsFilteringMode{
            if tableView != self.searchDisplayController!.searchResultsTableView {
                if !self.sections[section].items.isEmpty {
                    return self.collation.sectionTitles[section] as? String
                }
            }
        }
        return ""
    }
    
    override func sectionIndexTitlesForTableView(tableView: UITableView) -> [AnyObject]! {
        if !self.groupsFilteringMode{
            if tableView == self.searchDisplayController!.searchResultsTableView {
                return nil
            } else {
                return self.collation.sectionIndexTitles
            }
        }
        return nil
    }
    
    override func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        return self.collation.sectionForSectionIndexTitleAtIndex(index)
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if !self.groupsFilteringMode{
            if tableView == self.searchDisplayController!.searchResultsTableView {
                return self.filteredContacts.count
            } else {
                return self.sections[section].items.count
            }
        }else{
            if tableView == self.searchDisplayController!.searchResultsTableView {
                return self.filteredContacts.count
            } else {
                return self.contacts.count
            }
        }
    }
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if !self.groupsFilteringMode{
            if tableView != self.searchDisplayController!.searchResultsTableView {
                let header :UITableViewHeaderFooterView = view as UITableViewHeaderFooterView
                header.textLabel.textColor = UIColorFromHex(0x9E9E9E, alpha: 1)
                header.textLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
                header.contentView.backgroundColor = UIColorFromHex(0xF5F5F5, alpha: 1)
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !self.groupsFilteringMode{
            if tableView != self.searchDisplayController!.searchResultsTableView {
                if self.sections[section].items.count > 0 {
                    return 30
                }
            }
        }
        return 0
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cell = self.tableView!.dequeueReusableCellWithIdentifier("contactCell") as ContactCell
        if cell.tag == 1{
            cell.activityIndicator.startAnimating()
            return cell
        }
        if (tableView == self.searchDisplayController!.searchResultsTableView) {

            var cell = self.tableView!.dequeueReusableCellWithIdentifier("contactCell") as ContactCell
            var item : Contact
            item = self.filteredContacts[indexPath.row]

            cell.nameLabel.text = item.fname + " " + item.lname
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            if (item.merged){
                cell.nameLabel.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                cell.nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
                cell.emailIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                cell.mobileIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                cell.statusIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 0.8)
                cell.statusIcon.text = "\u{E803}"
                cell.textLabel?.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
            } else {
                cell.nameLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17.0)
                cell.nameLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
                cell.textLabel?.textColor = UIColorFromHex(0xB7AEAC, alpha: 1)
                cell.backgroundColor = UIColorFromHex(0xFFFFFF, alpha: 1)
                cell.emailIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                cell.mobileIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                cell.statusIcon.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                cell.statusIcon.text = "\u{E804}"
            }
            
            if item.phones.count > 0 && item.emails.count > 0 {
                cell.mobileIcon.text = "\u{E801}"
                cell.emailIcon.text = "\u{E800}"
            }else if item.phones.count > 0 && item.emails.count == 0 {
                cell.mobileIcon.text = "\u{E801}"
                cell.emailIcon.text = ""
            }else if item.phones.count == 0 && item.emails.count > 0 {
                cell.mobileIcon.text = "\u{E800}"
                cell.emailIcon.text = ""
            }else if item.phones.count == 0 && item.emails.count == 0 {
                cell.mobileIcon.text = ""
                cell.emailIcon.text = ""
            }

            cell.bottomBar.hidden = false
            
            return cell

        } else if (tableView == self.tableView) {
            
            if self.groupsFilteringMode {
                var cell = self.tableView!.dequeueReusableCellWithIdentifier("contactCell") as ContactCell
                var item : Contact
                item = self.contacts[indexPath.row]
                
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                
                if (item.merged){
                    cell.nameLabel.text = item.fname + " " + item.lname
                    cell.nameLabel.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                    cell.nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
                    cell.emailIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                    cell.mobileIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                    cell.statusIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 0.8)
                    cell.statusIcon.text = "\u{E803}"
                    cell.textLabel?.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                } else {
                    cell.nameLabel.text = item.fname + " " + item.lname
                    cell.nameLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17.0)
                    cell.nameLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.textLabel?.textColor = UIColorFromHex(0xB7AEAC, alpha: 1)
                    cell.backgroundColor = UIColorFromHex(0xFFFFFF, alpha: 1)
                    cell.emailIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.mobileIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.statusIcon.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                    cell.statusIcon.text = "\u{E804}"
                }
                
                if item.phones.count > 0 && item.emails.count > 0 {
                    cell.mobileIcon.text = "\u{E801}"
                    cell.emailIcon.text = "\u{E800}"
                }else if item.phones.count > 0 && item.emails.count == 0 {
                    cell.mobileIcon.text = "\u{E801}"
                    cell.emailIcon.text = ""
                }else if item.phones.count == 0 && item.emails.count > 0 {
                    cell.mobileIcon.text = "\u{E800}"
                    cell.emailIcon.text = ""
                }else if item.phones.count == 0 && item.emails.count == 0 {
                    cell.mobileIcon.text = ""
                    cell.emailIcon.text = ""
                }
                
                
                cell.bottomBar.hidden = false
                
                return cell
            } else {
                var cell = self.tableView!.dequeueReusableCellWithIdentifier("contactCell") as ContactCell
                var item = self.sections[indexPath.section].items[indexPath.row]
                
                cell.nameLabel.text = item.fname + " " + item.lname
                cell.selectionStyle = UITableViewCellSelectionStyle.None
                
                if (item.merged){
                    cell.nameLabel.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                    cell.nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
                    cell.emailIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                    cell.mobileIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                    cell.statusIcon.textColor = UIColorFromHex(0x66BB6A, alpha: 0.8)
                    cell.statusIcon.text = "\u{E803}"
                    cell.textLabel?.textColor = UIColorFromHex(0x66BB6A, alpha: 1)
                } else {
                    cell.nameLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17.0)
                    cell.nameLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.textLabel?.textColor = UIColorFromHex(0xB7AEAC, alpha: 1)
                    cell.backgroundColor = UIColorFromHex(0xFFFFFF, alpha: 1)
                    cell.emailIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.mobileIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.statusIcon.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                    cell.statusIcon.text = "\u{E804}"
                }
                
                if item.phones.count > 0 && item.emails.count > 0 {
                    cell.mobileIcon.text = "\u{E801}"
                    cell.emailIcon.text = "\u{E800}"
                }else if item.phones.count > 0 && item.emails.count == 0 {
                    cell.mobileIcon.text = "\u{E801}"
                    cell.emailIcon.text = ""
                }else if item.phones.count == 0 && item.emails.count > 0 {
                    cell.mobileIcon.text = "\u{E800}"
                    cell.emailIcon.text = ""
                }else if item.phones.count == 0 && item.emails.count == 0 {
                    cell.mobileIcon.text = ""
                    cell.emailIcon.text = ""
                }

                cell.bottomBar.hidden = true
                if indexPath.row < (self.sections[indexPath.section].items.count - 1){
                    cell.bottomBar.hidden = false
                } else {
                    cell.bottomBar.hidden = true
                }
                
                return cell
            }
        }
        return cell
    }
    
    
    func performSync(tableView: UITableView, record: Guest, indexPath: NSIndexPath, mode: String, action: String, mergeMode: String){
        guestCollection.performSync(record, action: action){
            (response) in
            if response["_id"] != nil{
                dispatch_async(dispatch_get_main_queue()){
                    record.id = response["_id"] as String
                    guestCollection.addGuest(record)
                    if mergeMode == "search"{
                        self.searchDisplayController!.searchResultsTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.None)
                        var cell = self.searchDisplayController!.searchResultsTableView.cellForRowAtIndexPath(indexPath) as ContactCell
                        cell.nameLabel.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
                        cell.emailIcon.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.mobileIcon.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.statusIcon.textColor = self.UIColorFromHex(0x66BB6A, alpha: 0.8)
                        cell.statusIcon.text = "\u{E803}"
                        cell.textLabel?.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.statusIcon.hidden = false
                        cell.activityIndicator.stopAnimating()
                        cell.tag = 0
                    } else {
                        var cell = tableView.cellForRowAtIndexPath(indexPath) as ContactCell
                        cell.nameLabel.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.nameLabel.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
                        cell.emailIcon.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.mobileIcon.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.statusIcon.textColor = self.UIColorFromHex(0x66BB6A, alpha: 0.8)
                        cell.statusIcon.text = "\u{E803}"
                        cell.textLabel?.textColor = self.UIColorFromHex(0x66BB6A, alpha: 1)
                        cell.statusIcon.hidden = false
                        cell.activityIndicator.stopAnimating()
                        cell.tag = 0
                    }
                    
                    
                }
            }
            if response["error"] != nil{
                dispatch_async(dispatch_get_main_queue()){
                    if mergeMode == "search"{
                        var cell = self.searchDisplayController!.searchResultsTableView.cellForRowAtIndexPath(indexPath) as ContactCell
                        cell.tag = 0
                        cell.statusIcon.hidden = false
                        cell.statusIcon.text = "\u{E805}"
                        cell.statusIcon.textColor = self.UIColorFromHex(0xF62821, alpha: 0.8)
                        cell.activityIndicator.stopAnimating()
                    } else {
                        var cell = tableView.cellForRowAtIndexPath(indexPath) as ContactCell
                        cell.tag = 0
                        cell.statusIcon.hidden = false
                        cell.statusIcon.text = "\u{E805}"
                        cell.statusIcon.textColor = self.UIColorFromHex(0xF62821, alpha: 0.8)
                        cell.activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        if self.groupsFilteringMode {
            if tableView == self.tableView{
                var cell = tableView.cellForRowAtIndexPath(indexPath) as ContactCell
                var item = self.contacts[indexPath.row]
                
                if self.shouldPerformSegueWithIdentifier("showMergeSegue", sender: tableView){
//                    self.searchDisplayController!.setActive(false, animated: false)
                    self.performSegueWithIdentifier("showMergeSegue", sender: tableView)
                }
                
            } else if tableView == self.searchDisplayController!.searchResultsTableView {
                var cell = tableView.cellForRowAtIndexPath(indexPath) as ContactCell
                var item = self.filteredContacts[indexPath.row]
                self.cleanHeaders()
                if self.shouldPerformSegueWithIdentifier("showMergeSegue", sender: tableView){
//                    self.searchDisplayController!.setActive(false, animated: false)
                    self.performSegueWithIdentifier("showMergeSegue", sender: tableView)
                }
            }
            
        } else {
            if tableView == self.tableView{
                var cell = tableView.cellForRowAtIndexPath(indexPath) as ContactCell
                var item = self.sections[indexPath.section].items[indexPath.row]
                
                if self.shouldPerformSegueWithIdentifier("showMergeSegue", sender: tableView){
//                    self.searchDisplayController!.setActive(false, animated: false)
                    self.performSegueWithIdentifier("showMergeSegue", sender: tableView)

                }
            } else if tableView == self.searchDisplayController!.searchResultsTableView {
                var cell = tableView.cellForRowAtIndexPath(indexPath) as ContactCell
                var item = self.filteredContacts[indexPath.row]
                self.cleanHeaders()
                if self.shouldPerformSegueWithIdentifier("showMergeSegue", sender: tableView){
//                    self.searchDisplayController!.setActive(false, animated: false)
                    self.performSegueWithIdentifier("showMergeSegue", sender: tableView)
                }
            }
            
        }
        
    }
    
    // Segue-related methods
    
    // Checks if the selected contact has a duplicate entry in the littleknot guests
    override func shouldPerformSegueWithIdentifier(identifier: String!, sender: AnyObject?) -> Bool {
        
        if (identifier == "showMergeSegue"){
            
            if sender as? UITableView == self.searchDisplayController!.searchResultsTableView {
                let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!

                var item = self.filteredContacts[indexPath.row]
                if !item.merged{
                    return true
                }
                
            } else if sender as? UITableView == self.tableView {
                if self.groupsFilteringMode {
                    
                    let indexPath = self.tableView.indexPathForSelectedRow()!
                    var item = self.contacts[indexPath.row]
                    if !item.merged{
                        return true
                    }

                } else {
                    let indexPath = self.tableView.indexPathForSelectedRow()!
                    var item = self.sections[indexPath.section].items[indexPath.row]
                    
                    if !item.merged{
                        return true
                    }
                }
            }
            
        }else if (identifier == "showGroups"){
            return true
        }
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Segue for contact cell
        if segue.identifier == "showMergeSegue"{
            let importViewController: ImportViewController = segue.destinationViewController as ImportViewController
            
            if sender as? UITableView == self.searchDisplayController!.searchResultsTableView {
                let indexPath = self.searchDisplayController!.searchResultsTableView.indexPathForSelectedRow()!
                importViewController.contact = self.filteredContacts[indexPath.row]
                importViewController.selectedPath = indexPath
                importViewController.mergeMode = "search"
            }else{
                let indexPath = self.tableView.indexPathForSelectedRow()!
                if self.groupsFilteringMode {
                    var item = self.contacts[indexPath.row]
                    importViewController.contact = item
                    importViewController.selectedPath = indexPath
                    importViewController.mergeMode = "groups"
                } else {
                    var item = self.sections[indexPath.section].items[indexPath.row]
                    var contact = Contact()
                    contact.fname = item.fname
                    contact.lname = item.lname
                    contact.names = item.names
                    contact.emails = item.emails
                    contact.phones = item.phones
                    contact.recordID = item.recordID
                    contact.guestID = item.guestID
                    contact.ccode = item.ccode
                    importViewController.contact = contact
                    importViewController.selectedPath = indexPath
                    importViewController.mergeMode = "contacts"
                }
            }
            importViewController.senderRow = sender as AnyObject
            importViewController.delegate = self
        } else if (segue.identifier == "showGroups"){
            let groupsTableViewController: GroupsTableController = segue.destinationViewController as GroupsTableController
            groupsTableViewController.delegate = self
        }
    }
    
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func determineStatus() -> Bool{
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
        case .Authorized:
            println("Authorized")
            return self.createAddressBook()
        case .NotDetermined:
            println("Not Determined")
            var ok = false
            ABAddressBookRequestAccessWithCompletion(nil) {
                (granted:Bool, err:CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if granted {
                        println("Granted")
                        ok = self.createAddressBook()
                        println("Getting all contact names")
                        self.getAllContactNames()
                    }
                }
            }
            if ok == true {
                return true
            }
            self.adbk = nil
            return false
        case .Restricted:
            println("Restricted")
            self.adbk = nil
            return false
        case .Denied:
            println("Denied")
            self.adbk = nil
            return false
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        if let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx) {
            return emailTest.evaluateWithObject(testStr)
        }
        return false
    }
    
}

