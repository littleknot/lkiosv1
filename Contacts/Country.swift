//
//  Country.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 3/31/15.
//  Copyright (c) 2015 Littleknot. All rights reserved.
//

import Foundation

class Country{
    var code: String;
    var name: String;
    
    init(code: String, name: String){
        self.code = code
        self.name = name
    }
}