//
//  DetailItem.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 12/11/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

class DetailItem{
    var name: NSString
    var selected: Bool
    var type: String
    var section: String
    var actionType: String
    
    init(name:NSString, selected: Bool, type: String, section: String, actionType: String){
        self.name = name
        self.selected = selected
        self.type = type
        self.section = section
        self.actionType = actionType
    }
    
}