//
//  Item.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 12/3/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

class Item: NSObject{
    var name: NSString
    var fname: NSString
    var lname: NSString
    var ccode: NSString
    var names = [NSString]()
    var phones = [NSString]()
    var emails = [NSString]()
    var section: Int?
    var merged: Bool
    var recordID = Int()
    var guestID = NSString()
    
    
    init(name: NSString, fname: NSString, lname: NSString, phones: [NSString], emails: [NSString], recordID: Int, merged: Bool) {
        self.name = name
        self.fname = fname
        self.lname = lname
        self.phones = phones
        self.emails = emails
        self.merged = merged
        self.recordID = recordID
        self.ccode = ""
    }
}