//
//  Guest.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/15/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import Foundation

class Guest{
    var id:String
    var fname:String
    var lname:String
    var email:String
    var phone:String
    var ccode:String
    var contactID:String
    
    init(){
        self.id = ""
        self.fname = ""
        self.lname = ""
        self.email = ""
        self.phone = ""
        self.ccode = ""
        self.contactID = ""
    }
    
    func destroy() -> Bool{
        return true
    }
    
    func toJSON() -> String{
        return ""
    }
    
    func isValid() -> Bool{
        if (fname != "") {
            return true
        }
        return false
    }
    
    func getName() -> String{
        return "\(self.fname) \(self.lname)"
    }
}
