//
//  ConnectionManager.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 12/23/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit

var connection: ConnectionManager = ConnectionManager()

class ConnectionManager: NSObject{
    
    var settings: Settings!
    
    override init(){
        self.settings = Settings()
    }
    
    func auth(url:String, fbid:String, callback:(NSDictionary) ->()){
//        println("auth called")
        var req = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        
        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
        req.addValue("application/json", forHTTPHeaderField: "Accept")
        req.addValue(settings.APIKey, forHTTPHeaderField: "X-LITTLEKNOT-API-KEY")
        req.addValue(fbid, forHTTPHeaderField: "X-LITTLEKNOT-FB-ID")
        req.HTTPMethod = "POST"
        
        
        var task = NSURLSession.sharedSession().dataTaskWithRequest(req){
            (data, response, error) in
            println("auth returned")
            var error: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &error) as? NSDictionary

            if(error != nil) {
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
            }else{
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    callback(parseJSON)
                }
                else {
                    // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                }
            }
        }
        task.resume()
    }

    
    func get(url:String, userKey:String, accountKey:String,  callback:(NSDictionary) ->()){
//        println("get called")
        var req = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        
        req.addValue(settings.APIKey, forHTTPHeaderField: "X-LITTLEKNOT-API-KEY")
        req.addValue(userKey, forHTTPHeaderField: "X-LITTLEKNOT-USER-KEY")
        req.addValue(accountKey, forHTTPHeaderField: "X-LITTLEKNOT-ACCOUNT-KEY")
        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
        req.addValue("application/json", forHTTPHeaderField: "Accept")
        
        req.HTTPMethod = "GET"
        
        
        var task = NSURLSession.sharedSession().dataTaskWithRequest(req){
            (data, response, error) in
//            println("get returned")
            var error: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &error) as? NSDictionary
            
            if(error != nil) {
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
            }else{
                // The JSONObjectWithData constructor didn't return an error. But, we should still
                // check and make sure that json has a value using optional binding.
                if let parseJSON = json {
                    callback(parseJSON)
                }
                else {
                    // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                    callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
                }
            }
        }
        task.resume()
    }
    
    func post(params : Dictionary<String, String>, userKey:String, accountKey:String, url : String, callback:(NSDictionary) ->() ){
        var req = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()

        req.HTTPMethod = "POST"
        
        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
        req.addValue("application/json", forHTTPHeaderField: "Accept")
        req.addValue(settings.APIKey, forHTTPHeaderField: "X-LITTLEKNOT-API-KEY")
        req.addValue(userKey, forHTTPHeaderField: "X-LITTLEKNOT-USER-KEY")
        req.addValue(accountKey, forHTTPHeaderField: "X-LITTLEKNOT-ACCOUNT-KEY")
        
        var err: NSError?
        req.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        var task = session.dataTaskWithRequest(req, completionHandler: {data, response, error -> Void in

            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)

            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary

            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
            }
            else {
                if let parseJSON = json {
                    callback(parseJSON)
                }
                else {
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                    callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
                }
            }
        })
        
        task.resume()
    }
    
    func patch(params : Dictionary<String, String>, userKey:String, accountKey:String, url : String, callback:(NSDictionary) ->() ){
        var req = NSMutableURLRequest(URL: NSURL(string: url)!)
        var session = NSURLSession.sharedSession()
        
        req.HTTPMethod = "PATCH"
        
        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
        req.addValue("application/json", forHTTPHeaderField: "Accept")
        req.addValue(settings.APIKey, forHTTPHeaderField: "X-LITTLEKNOT-API-KEY")
        req.addValue(userKey, forHTTPHeaderField: "X-LITTLEKNOT-USER-KEY")
        req.addValue(accountKey, forHTTPHeaderField: "X-LITTLEKNOT-ACCOUNT-KEY")
        
        var err: NSError?
        req.HTTPBody = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        var task = session.dataTaskWithRequest(req, completionHandler: {data, response, error -> Void in
            
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
            
            // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
            if(err != nil) {
                println(err!.localizedDescription)
                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("Error could not parse JSON: '\(jsonStr)'")
                callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
            }
            else {
                if let parseJSON = json {
                    callback(parseJSON)
                }
                else {
                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("Error could not parse JSON: \(jsonStr)")
                    callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
                }
            }
        })
        
        task.resume()
    }
    
//    func del(url : String, userKey:String, accountKey:String, callback:(NSDictionary) ->() ){
//        var req = NSMutableURLRequest(URL: NSURL(string: url)!)
//        var session = NSURLSession.sharedSession()
//        
//        req.addValue(settings.APIKey, forHTTPHeaderField: "X-LITTLEKNOT-API-KEY")
//        req.addValue(userKey, forHTTPHeaderField: "X-LITTLEKNOT-USER-KEY")
//        req.addValue(accountKey, forHTTPHeaderField: "X-LITTLEKNOT-ACCOUNT-KEY")
//        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        req.addValue("application/json", forHTTPHeaderField: "Accept")
//        
//        req.HTTPMethod = "DELETE"
//        
//        
//        var task = NSURLSession.sharedSession().dataTaskWithRequest(req){
//            (data, response, error) in
//            var error: NSError?
//            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &error) as? NSDictionary
//            
//            if(error != nil) {
//                let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
//                println("Error could not parse deleted JSON: '\(jsonStr)'")
//                callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
//            }else{
//                // The JSONObjectWithData constructor didn't return an error. But, we should still
//                // check and make sure that json has a value using optional binding.
//                if let parseJSON = json {
//                    callback(parseJSON)
//                }
//                else {
//                    // Woa, okay the json object was nil, something went worng. Maybe the server isn't running?
//                    let jsonStr = NSString(data: data, encoding: NSUTF8StringEncoding)
//                    println("Error could not parse JSON: \(jsonStr)")
//                    callback(["error": self.settings.alertMessage("unreachable")] as NSDictionary)
//                }
//            }
//        }
//        task.resume()
//    }
}
