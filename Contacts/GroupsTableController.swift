//
//  GroupsTableController.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/13/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit
import AddressBook


// Delegates
protocol DataSelectGroupDelegate{
    func userDidSelectGroup(controller: GroupsTableController, record: ABRecord?, showFiltered: Bool)
}

class GroupsTableController: UITableViewController, UITableViewDelegate, UITableViewDataSource{
    
    var adbk: ABAddressBookRef!
    
    var delegate: DataSelectGroupDelegate? = nil
    
    var groups = [Group]()
    
    var filteredGroups = [Group]()
    
    @IBOutlet var groupsCollection: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getGroupNames()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
//        self.groupsCollection.reloadData()
    }
    
    func getGroupNames(){
        if !self.determineStatus(){
            println("not authorized")
        }else{

            self.groups.append(Group(name: "All"))
            let groupList = ABAddressBookCopyArrayOfAllGroups(self.adbk).takeRetainedValue() as NSArray as [ABRecord]
            for group in groupList{

                let id = ABRecordGetRecordID(group) as ABRecordID
                let name = ABRecordCopyValue(group, kABGroupNameProperty).takeRetainedValue() as NSString
                var groupObject = Group(name: name)
                groupObject.id = id
                groupObject.obj = group
            
                self.groups.append(groupObject)
                dispatch_async(dispatch_get_main_queue()){
                    self.groupsCollection.reloadData()
                }
            }
        }
    }
    
    func createAddressBook() -> Bool {
        if self.adbk != nil {
            return true
        }
        var err : Unmanaged<CFError>? = nil
        let adbk : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &err).takeRetainedValue()
        if adbk == nil {
            println(err)
            self.adbk = nil
            return false
        }
        self.adbk = adbk
        return true
    }
    
    
    func determineStatus() -> Bool{
        let status = ABAddressBookGetAuthorizationStatus()
        switch status {
        case .Authorized:
            return self.createAddressBook()
        case .NotDetermined:
            var ok = false
            ABAddressBookRequestAccessWithCompletion(nil) {
                (granted:Bool, err:CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if granted {
                        ok = self.createAddressBook()
                    }
                }
            }
            if ok == true {
                return true
            }
            self.adbk = nil
            return false
        case .Restricted:
            self.adbk = nil
            return false
        case .Denied:
            self.adbk = nil
            return false
        }
    }
    
    //UITableView
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        println(editingStyle)
        if(editingStyle == UITableViewCellEditingStyle.Delete){
            self.groupsCollection.reloadData()
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.groups.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "test")
        cell.textLabel?.text = groups[indexPath.row].name
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.textLabel?.font = UIFont.systemFontOfSize(16.0)
        cell.textLabel?.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
        
        return cell
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var item = self.groups[indexPath.row]
        var showFiltered = true
        if item.name == "All"{
            showFiltered = false
        }
        delegate!.userDidSelectGroup(self, record: item.obj, showFiltered: showFiltered)
    }
    
}

