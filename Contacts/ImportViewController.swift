//
//  ImportViewController.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 11/16/14.
//  Copyright (c) 2014 Ian Bert Tusil. All rights reserved.
//

import UIKit
import CoreData
import CoreTelephony

let managedCountryContext = (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext

// Delegates
protocol DataMergedDelegate{
    func userDidMergeContactInformation(controller: ImportViewController, guest: Guest, selectedPath: NSIndexPath, action: String, mergeMode: String)
}

class ImportViewController: UITableViewController, UITableViewDelegate, UITableViewDataSource, CountrySelectDelegate{
    
    let cellIdentifier = "cellItemIdentifier"
    
    var delegate: DataMergedDelegate? = nil
    
    @IBOutlet weak var actionButton: UIBarButtonItem!
    @IBOutlet weak var buttonView: UIView!
    var contact = Contact()
    var items = [DetailItem]()
    var selectedPath = NSIndexPath()
    var senderRow: AnyObject!
    var hasConflict = false
    var hasConflictedName = false
    var hasConflictedEmail = false
    var hasConflictedNumber = false
    var countryCode = ""
    var mergeMode = ""
    
    
    @IBOutlet weak var checkIcon: UILabel!
    @IBOutlet var itemsCollection: UITableView!
    
//    @IBOutlet var countriesCollection: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if contact.phones.count > 0{
            contact.ccode = getCountryCode(contact.phones[0])
        }
        
        self.itemsCollection?.registerClass(UITableViewCell.self, forCellReuseIdentifier: self.cellIdentifier)
        items.append(generateSeparator())
        items.append(generateLabel("Name"))
        if (contact.names.count > 1){
            self.hasConflict = true
            self.hasConflictedName = true
            items.append(generateConflictWarning("name"))
            var actions = ["Add", "Merge"]
            for (index, item) in enumerate(contact.names){
                self.items.append(DetailItem(name: item, selected: false, type: "cellName", section: "name", actionType: actions[index]))
            }
        }else if (contact.names.count == 1){
            self.items.append(DetailItem(name: contact.names[0], selected: true, type: "cell", section: "name", actionType: "Add"))
        }
        if contact.emails.count > 0 {
            items.append(generateSeparator())
            items.append(generateLabel("Email"))
            if (contact.emails.count > 1){
                self.hasConflict = true
                self.hasConflictedEmail = true
                items.append(generateConflictWarning("email"))
                for item in contact.emails{
                    self.items.append(DetailItem(name: item, selected: false, type: "cell", section: "email", actionType: ""))
                }
            } else if (contact.emails.count == 1){
                self.items.append(DetailItem(name: contact.emails[0], selected: true, type: "cell", section: "email", actionType: ""))
            }
        }
        
        if (contact.phones.count > 0){
            items.append(generateSeparator())
            items.append(generateLabel("Mobile"))
            if (contact.phones.count > 1){
                self.hasConflict = true
                self.hasConflictedNumber = true
                items.append(generateConflictWarning("number"))
                for item in contact.phones{
                    self.items.append(DetailItem(name: item, selected: false, type: "cell", section: "number", actionType: ""))
                }
            } else if (contact.phones.count == 1) {
                self.items.append(DetailItem(name: contact.phones[0], selected: true, type: "cell", section: "number", actionType: ""))
            }
        }
        
        if contact.phones.count > 0{
            items.append(generateSeparator())
            items.append(generateLabel("Country Code"))
            self.items.append(DetailItem(name: contact.ccode, selected: false, type: "ccode", section: "ccode", actionType: ""))
        }
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        if self.hasConflict{
            actionButton.title = ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.itemsCollection.reloadData()
    }
    
    func userDidSelectCountry(controller: CountriesTableController, selectedPath: NSIndexPath){
        
//        var row = senderRow as DetailCell
//        row.ccodeLabel.text = "+\(countryCollection.countries[selectedPath.row].code)"
        self.contact.ccode = "+\(countryCollection.countries[selectedPath.row].code)"
        dispatch_async(dispatch_get_main_queue()) {
            self.itemsCollection.reloadData()
        }
        controller.navigationController?.popViewControllerAnimated(true)
    }

    func insertContact(recordID: Int, guestID: String) {
        var appDel:AppDelegate = (UIApplication.sharedApplication().delegate as AppDelegate)
        var context:NSManagedObjectContext = appDel.managedObjectContext!
        
        var newContact = NSEntityDescription.insertNewObjectForEntityForName("Contacts", inManagedObjectContext: context) as NSManagedObject
        
        newContact.setValue(recordID, forKey: "recordID")
        newContact.setValue(guestID, forKey: "guestID")
        context.save(nil)
    }
    
    @IBAction func importPressed(sender: AnyObject) {
        if delegate != nil{
            var guest = Guest()
            guest.fname = contact.fname
            guest.lname = contact.lname
            guest.ccode = contact.ccode
            guest.contactID = "\(contact.recordID)"
            guest.id = "\(contact.guestID)"
            
            for item in self.items{
                if item.section == "email" && item.selected{
                    guest.email = item.name
                }
                if item.section == "number" && item.selected{
                    guest.phone = item.name
                }
            }
            
            if guest.isValid(){
                self.delegate!.userDidMergeContactInformation(self, guest: guest, selectedPath: self.selectedPath, action: actionButton.title!, mergeMode: self.mergeMode)
            }else{
                let alertController = UIAlertController(title: "Required Missing", message: "Contact should have a name", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func generateSeparator() -> DetailItem{
        return DetailItem(name: "", selected: false, type: "separator", section: "", actionType: "")
    }
    func generateLabel(name: String) -> DetailItem{
        return DetailItem(name: name, selected: false, type: "label", section: "", actionType: "")
    }
    func generateConflictWarning(section: String) -> DetailItem{
        if section == "name"{
            return DetailItem(name: "", selected: false, type: "conflictName", section: section, actionType: "")
        }
        return DetailItem(name: "", selected: false, type: "conflict", section: section, actionType: "")
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        var cellRow = self.items[indexPath.row]
        if(cellRow.type == "cell" || cellRow.type == "cellName" || cellRow.type == "ccode"){
            let bottomBar = UIView(frame: CGRect(x: 16, y: 43, width: tableView.bounds.width - 32, height: 1))
            bottomBar.backgroundColor = UIColorFromHex(0xE0E0E0, alpha: 1)
            cell.addSubview(bottomBar)
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        var cellRow = self.items[indexPath.row]
        var identifier = "detailSeparator"
        if (cellRow.type == "cell"){
            identifier = "detailCell"
        }else if (cellRow.type == "cellName"){
            identifier = "detailCellName"
        }else if (cellRow.type == "label"){
            identifier = "detailLabel"
        }else if (cellRow.type == "conflict"){
            identifier = "detailConflict"
        }else if (cellRow.type == "conflictName"){
            identifier = "detailNameConflict"
        }else if (cellRow.type == "ccode"){
            identifier = "detailCountryCode"
        }
        let cell = self.tableView!.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as DetailCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        if (cellRow.type == "ccode"){
            cell.ccodeLabel.text = contact.ccode
            cell.ccodeLabel.textColor = UIColorFromHex(0x212121, alpha: 1)

        }else if (cellRow.type == "cell"){
            if (cellRow.selected == true){
                cell.checkIcon.text = "\u{E806}"
            }
            
            cell.itemLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
            cell.checkIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
            
            if (self.contact.names.count > 1 && cellRow.type == "cell" && cellRow.section == "name"){
                if (cellRow.selected == true){
                    cell.itemLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
//                    cell.checkIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                } else {
                    cell.checkIcon.text = ""
//                    cell.checkIcon.textColor = UIColorFromHex(0x212121, alpha: 1)
                    cell.itemLabel.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                }
            }
            
            if (self.contact.phones.count > 1 && cellRow.type == "cell" && cellRow.section == "number"){
                if (cellRow.selected == true){
                    cell.itemLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
//                    cell.checkIcon.textColor = UIColorFromHex(0x9E9E9E, alpha: 1)
                } else {
                    cell.checkIcon.text = ""
                    cell.itemLabel.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                }
            }
            
            if (self.contact.emails.count > 1 && cellRow.type == "cell" && cellRow.section == "email"){
                if (cellRow.selected == true){
                    cell.itemLabel.textColor = UIColorFromHex(0x212121, alpha: 1)
//                    cell.checkIcon.textColor = UIColorFromHex(0x9E9E9E, alpha: 1)
                } else {
                    cell.checkIcon.text = ""
                    cell.itemLabel.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                }
            }
            
            cell.itemLabel.text = self.items[indexPath.row].name
        } else if (cellRow.type == "cellName"){
            if (cellRow.selected == true){
                cell.checkIconName.text = "\u{E806}"
            }
            
            cell.itemLabelName.textColor = UIColorFromHex(0x212121, alpha: 1)
            cell.checkIconName.textColor = UIColorFromHex(0x212121, alpha: 1)
            cell.cellDescription.textColor = UIColorFromHex(0xBDBDBD, alpha: 1)
            
            
            if cellRow.actionType == "Merge"{
                cell.cellDescription.text = "(merge with LittleKnot guest)"
            } else if cellRow.actionType == "Add"{
                cell.cellDescription.text = "(add as new guest)"
            }
            
            
            if (self.contact.names.count > 1 && cellRow.type == "cellName" && cellRow.section == "name"){
                
                if (cellRow.selected == true){
                    cell.itemLabelName.textColor = UIColorFromHex(0x212121, alpha: 1)
//                    cell.checkIconName.textColor = UIColorFromHex(0x9E9E9E, alpha: 1)
                } else {
                    cell.checkIconName.text = ""
                    cell.itemLabelName.textColor = UIColorFromHex(0x00BCD4, alpha: 1)
                }
            }
            cell.itemLabelName.text = self.items[indexPath.row].name
        } else if (cellRow.type == "label"){
            cell.labelName.textColor = UIColorFromHex(0x9E9E9E, alpha: 1)
            cell.labelName.text = self.items[indexPath.row].name
        } else if (cellRow.type == "conflict"){
            cell.conflictSign.text = "\u{E805}"
            cell.conflictSign.textColor = UIColorFromHex(0xF44336, alpha: 1)
            cell.conflictMessage.textColor = UIColorFromHex(0xF44336, alpha: 1)
        } else if (cellRow.type == "conflictName"){
            cell.conflictNameSign.text = "\u{E805}"
            cell.conflictNameSign.textColor = UIColorFromHex(0xF44336, alpha: 1)
            cell.conflictNameMessage.textColor = UIColorFromHex(0xF44336, alpha: 1)
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var item = self.items[indexPath.row]
        if (item.type == "separator"){
            return 16
        } else if (item.type == "label"){
            return 24
        } else if (item.type == "conflict"){
            return 24
        } else if (item.type == "conflictName"){
            return 40
        }
        return 44
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var item = self.items[indexPath.row]
        if (item.type == "cell" || item.type == "cellName"){
            var currentCell = tableView.cellForRowAtIndexPath(indexPath) as UITableViewCell?
            deselectSameTypeAndSection(item)
            item.selected = true
            
            if item.type == "cellName"{
                actionButton.title = item.actionType
                self.hasConflictedName = false
            } else if item.type == "cell"{
                if item.section == "email"{
                    self.hasConflictedEmail = false
                }
                if item.section == "number"{
                    self.hasConflictedNumber = false
                }
                println(self.hasConflictedName)
                println(self.hasConflictedNumber)
                println(self.hasConflictedNumber)
                if !self.hasConflictedName && !self.hasConflictedEmail && !self.hasConflictedNumber{
                    actionButton.title = "Add"
                }
            }
//            if !self.hasConflictedName{
//                actionButton.title = "Add"
//            }
            if (item.type == "cell") && (item.section == "number"){
              updateCountryCode(item.name)
            }
            self.itemsCollection.reloadData()
        }
    }
    
    func deselectSameTypeAndSection(item: DetailItem){
        var selectedType = item.type
        var selectedSection = item.section
        for (index, item) in enumerate(self.items){
            if (item.type == selectedType && item.section == selectedSection){
                item.selected = false
            }
            if (item.type == "conflict") && (item.section == selectedSection){
                self.items.removeAtIndex(index)
            }
            if (item.type == "conflictName") && (item.section == selectedSection){
                self.items.removeAtIndex(index)
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "showCountries" {
            let countriesTableController: CountriesTableController = segue.destinationViewController as CountriesTableController
            countriesTableController.senderRow = sender.senderRow
            countriesTableController.delegate = self
            
        }
    }
    
    func updateCountryCode(phone: String) {
        var ccode = ""
        for (index, item) in enumerate(self.items){
            if (item.type == "ccode" && item.section == "ccode") {
                contact.ccode = getCountryCode(phone)
            }
        }
    }
    
    func getCountryCode(phone: NSString) -> String{
        var ccode = ""
        var found = false
        var phone: String = phone
        if(phone.rangeOfString("+") == nil){
            var networkInformation: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
            if networkInformation.subscriberCellularProvider != nil{
                var ct: CTCarrier = networkInformation.subscriberCellularProvider
                
                for item in countryCollection.country_codes{
                    if item.name.lowercaseString == ct.isoCountryCode{
                        ccode = item.code
                        found = true
                        break;
                    }
                }
                if !found{
                    ccode = "1"
                } else {
                    found = false
                    for item in countryCollection.countries{
                        if item.code == ccode{
                            ccode = item.code
                            found = true
                            println(ccode)
                            break;
                        }
                    }
                    if !found{
                        ccode = "1"
                    }
                }
            } else {
                ccode = "1"
            }
            ccode = "+\(ccode)"
            
        }else{
            if (phone.rangeOfString("+")?.startIndex != nil){
                if phone.rangeOfString("+1") != nil{
                    ccode = "+1"
                }else if phone.rangeOfString("+65") != nil{
                    ccode = "+65"
                }else if phone.rangeOfString("+60") != nil{
                    ccode = "+60"
                }else if phone.rangeOfString("+61") != nil{
                    ccode = "+61"
                }else if phone.rangeOfString("+62") != nil{
                    ccode = "+62"
                }else if phone.rangeOfString("+63") != nil{
                    ccode = "+63"
                }else if phone.rangeOfString("+64") != nil{
                    ccode = "+64"
                }else if phone.rangeOfString("+91") != nil{
                    ccode = "+91"
                }else if phone.rangeOfString("+852") != nil{
                    ccode = "+852"
                } else {
                    var networkInformation: CTTelephonyNetworkInfo = CTTelephonyNetworkInfo()
                    if networkInformation.subscriberCellularProvider != nil{
                        var ct: CTCarrier = networkInformation.subscriberCellularProvider
                        for item in countryCollection.country_codes{
                            if item.name.lowercaseString == ct.isoCountryCode{
                                ccode = item.code
                                found = true
                                break;
                            }
                        }
                        if !found{
                            ccode = "1"
                        } else {
                            found = false
                            for item in countryCollection.countries{
                                if item.code == ccode{
                                    ccode = item.code
                                    found = true
                                    break;
                                }
                            }
                            if !found{
                                ccode = "1"
                            }
                        }
                    } else {
                        ccode = "1"
                    }
                    ccode = "+\(ccode)"
                }
            }
        }
        self.countryCode = ccode
        return ccode
    }
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func getisoCountryCodeToCountryCode() {
        
    }
}
