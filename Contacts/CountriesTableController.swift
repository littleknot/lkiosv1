//
//  CountriesTableController.swift
//  Contacts
//
//  Created by Ian Bert Tusil on 3/30/15.
//  Copyright (c) 2015 Littleknot. All rights reserved.
//

import UIKit
import AddressBook
import CoreData

protocol CountrySelectDelegate{
    func userDidSelectCountry(controller: CountriesTableController, selectedPath: NSIndexPath)
}

class CountriesTableController: UITableViewController, UITableViewDelegate, UITableViewDataSource {

    var delegate: CountrySelectDelegate? = nil
    
    var senderRow: AnyObject!
    
    @IBOutlet var countriesCollection: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
    }
    
    //UITableView
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return countryCollection.countries.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "test")
        cell.textLabel?.text = "\(countryCollection.countries[indexPath.row].name) (+\(countryCollection.countries[indexPath.row].code))"

        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.textLabel?.font = UIFont.systemFontOfSize(16.0)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.delegate!.userDidSelectCountry(self, selectedPath: indexPath)
    }
    
    
    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}